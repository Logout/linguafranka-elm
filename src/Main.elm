module Main exposing (main)

import Dict
import Browser
import Browser.Navigation as Nav
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Http
import Url



-- MAIN


main : Program () Model Msg
main =
  Browser.application
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    , onUrlChange = UrlChanged
    , onUrlRequest = LinkClicked
    }



-- MODEL



type Language = German
    | English
    | French
    | Italian
    | Spanish

languageToInt : Language -> Int
languageToInt language =
  case language of
    German -> 1
    English -> 2
    French -> 3
    Italian -> 4
    Spanish -> 5

type alias LanguageIdentifier =
    { key: String
    , representation: String
    }

supportedLanguages : Dict.Dict Int LanguageIdentifier
supportedLanguages = Dict.fromList [
    (languageToInt German, LanguageIdentifier "ger" "Deutsch"),
    (languageToInt English, LanguageIdentifier "eng" "English"),
    (languageToInt French, LanguageIdentifier "fra" "Français"),
    (languageToInt Italian, LanguageIdentifier "ita" "Italiano"),
    (languageToInt Spanish, LanguageIdentifier "esp" "Español")]

type alias Model =
  { key : Nav.Key
  , url : Url.Url
  , word: String
  , sourceLanguage: LanguageIdentifier
  , destinationLanguage: LanguageIdentifier
  , result: String
  }

init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
  ( Model key url "" defaultLanguage defaultLanguage "", Cmd.none )

getSupportedLanguage : Language -> LanguageIdentifier
getSupportedLanguage language =
    Maybe.withDefault
        (LanguageIdentifier "err" "Error mapping languages!")
        (Dict.get (languageToInt language) supportedLanguages)

defaultLanguage = getSupportedLanguage German


-- UPDATE


type Msg
  = LinkClicked Browser.UrlRequest
  | UrlChanged Url.Url
  | Search
  | WordChanged String
  | SourceLangChanged String
  | DestLangChanged String
  | DataReceived (Result Http.Error String)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
  case msg of
    LinkClicked urlRequest ->
      case urlRequest of
        Browser.Internal url ->
          ( model, Cmd.none )

        Browser.External href ->
          ( model, Nav.load href )

    UrlChanged url ->
        ( { model | url = url }, getLookupResponse model )

    Search ->
        ( model, Nav.pushUrl model.key (assembleSearchUrl model) )

    WordChanged word ->
        ( { model | word = word }, Cmd.none )

    SourceLangChanged sourceLanguage ->
        ( { model | sourceLanguage = stringToLanguage model.sourceLanguage sourceLanguage }, Cmd.none )

    DestLangChanged destinationLanguage ->
        ( { model | destinationLanguage = stringToLanguage model.destinationLanguage destinationLanguage }, Cmd.none )

    DataReceived result ->
        case result of
            Ok data ->
                ( { model | result = data }, Cmd.none )

            Err httpError ->
                ( model, Cmd.none )

assembleSearchUrl : Model -> String
assembleSearchUrl model = "#search&" ++ (assembleUrlParameters model)

assembleUrlParameters : Model -> String
assembleUrlParameters model = "word=" ++ model.word
    ++ "&from=" ++ model.sourceLanguage.key
    ++ "&to=" ++ model.destinationLanguage.key

stringToLanguage : LanguageIdentifier -> String -> LanguageIdentifier
stringToLanguage default optionInput =
    Dict.filter (\key value -> value.representation == optionInput) supportedLanguages
    |> Dict.toList
    |> List.head
    |> Maybe.withDefault (0, default)
    |> Tuple.second

server_url : Model -> String
server_url model = "http://localhost:8000/lookup?service=pons&" ++ (assembleUrlParameters model)

getLookupResponse : Model -> Cmd Msg
getLookupResponse model = Http.get
    { url = server_url model
    , expect = Http.expectString DataReceived
    }


-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
  Sub.none



-- VIEW
languageToOption : LanguageIdentifier -> Html msg
languageToOption lang = option [] [ text lang.representation ]

optionsForAllLanguages : List (Html msg)
optionsForAllLanguages = List.map languageToOption ( Dict.values supportedLanguages )

view : Model -> Browser.Document Msg
view model =
  { title = "Lingua Franka"
  , body =
      [ h1 [] [ text "Lingua Franka Meta Dictionary" ]
      , p [] [ text "What do you want to look up?" ]
      , p [] [ input [ onInput WordChanged ] [] ]
      , p []
        [ select [ size 1, onInput SourceLangChanged ] optionsForAllLanguages
        , text "↔"
        , select [ size 1, onInput DestLangChanged ] optionsForAllLanguages
        ]
      , p [] [ text ("Result: " ++ model.result) ]
      , p [] [ button [ onClick Search ] [ text "Search" ] ]
      ]
  }

